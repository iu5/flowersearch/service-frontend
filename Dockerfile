
FROM node:10.10

ENV APP_ROOT /src
ENV HOST 0.0.0.0
ENV API_URL http://backend:80
ENV STATIC_URL http://10.100.10.53:7766

RUN mkdir ${APP_ROOT}
WORKDIR ${APP_ROOT}
ADD . ${APP_ROOT}

EXPOSE 3000
RUN npm install
RUN npm run build