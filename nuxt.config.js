export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Flower Search',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Search the flower by the image | Practical Machine Learning & Deep Learning project.' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'swiper/swiper.scss',
    'swiper/swiper-bundle.min.css',
    '@/assets/styles/customBuefy.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {src: '@/plugins/swiper', ssr: false, mode: 'client' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/buefy
    'nuxt-buefy',
    '@nuxtjs/proxy',
    '@nuxtjs/axios'
  ],

  env: {
    API_URL: process.env.API_URL || 'http://10.100.10.53:7766',
    STATIC_URL: process.env.STATIC_URL || 'http://10.100.10.53:7766'
  },

  axios: {
    proxy: true
  },

  proxy: {
    '/api/': {
      target: process.env.API_URL || 'http://10.100.10.53:7766',
      pathRewrite: { '^/api/': '' }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
