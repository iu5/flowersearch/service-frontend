import Vue from 'vue'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import { Swiper, EffectCoverflow, Pagination, Keyboard } from 'swiper';

Swiper.use([EffectCoverflow, Pagination, Keyboard]);
Vue.use(VueAwesomeSwiper)